(ns tutorial.core
  (:gen-class)
  (:require [clojure.string :as str]))

(use 'clojure.java.io)

(defn -main
  "I don't do a whole lot ... yet."
  [& args])
  
(def str1 "this is the STRING that you will USE for MANIPULATION")
(def list1 (list "Cat" 8 481.51 true))
(def set1 '(1 2 3 4 5 6))
(def vector1 (vector 1 2 2 3 5 3 4))
(def hashmap1 (hash-map :a "Hello" :b "World" :c "This" :d "Is" :e "Me"))

(defn writeString
  ([]
   (println "If you've got something to say, re-write this function with what you wanted to say"))
  ([x]
   (format "I just had something I wanted to say: %s" x)))  

(defn messingWithStrings
  ([]
   (println "In this method we have only a single string but here are some of the functions that you can input to manipulated the string")
   (println "To view the string type in this (str str1)")
   (println "Test if the string is blank type (str/blank? str1)")
   (println "Test if the string includes another string tpye (str/includes? str1 \"{whatever you want in thsese quotes}\"")
   (println "Test what the index of an item in the string is type (str/index-of str1 \"{whatever string component you want to find the index of}\"")
   (println "Test what the string would look like with a split type (str/spit #\" \")")
   (println "Test what it would be like joining a string type (str/join \" \" [\"The components of the string in a hash map\"]")
   (println "Test what replacing part of a string would be like type (str/replace \"{Your test string here}\" #\"{what you want to replace}\" \"what you want to replace it to\"")
   (println "Turn the string all uppercase with (str/upper-case str1)")
   (println "Turn the string all lowercase with (str/lower-case str1)")))
   

(defn allTrue
  ([]
   (println "The method takes 5 parameters, you need to make sure they're all valid")
   (println "Those parameters (in order) are: a positive number, a negative number, a string, a float, and a zero")
   (println "The positive number will need to be even and an integer")
   (println "The negative number will need to be odd")
   (println "The string can be anything")
   (println "The float must of type float")
   (println "The zero must be zero in number form")) 
  ([positive negative string float zero]
   (= true
     (pos? positive)
     (neg? negative) 
     (even? positive)
     (odd? negative)
     (number? positive)
     (string? string)
     (integer? positive)
     (float? float)
     (zero? zero))))

(defn messWithMyList
  ([]
   (println "There is a list declared in the program. It is called list1")
   (println "Wanna see the list, type {println list1}")
   (println "Wanna get the first value, type {first list1}")
   (println "Wanna get the rest of the list, type {rest list1}")
   (println "Wanna get the last value of the list, type {last list1}")
   (println "Wanna get a certain value in the list, type {nth list1 2} where 2 is the nth item in the list array")
   (println "Wanna add to the list, type {list* 1 3 [2 4]} where 2 and 4 are the numbers added to the list")
   (println "Wanna add to the start of the list, type {cons [2 4] list1} where 2 and 4 are added to the start of the list")))

(defn messWithMySet
  ([]
   (println "A set if a grouping of items where each item is unique")
   (println "Wanna see the set, type {println set1}")
   (println "Wanna get the nth item in a set, type {get (set set1) 2} which would get the index of the second item in the set")
   (println "Wanna conjoin something to the set, type {conj (set set1) 7}. If you were to join a number already in the set, no change would occur")
   (println "Wanna see if the set contains a certain value {contains? (set set1) 2}")
   (println "Wanna remove a value from a set, type {disj (set set1) 2}")))

(defn messWithMyVector
  ([]
   (println "A vector is a of items")
   (println "Wanna see the vector, type {println vector1}")
   (println "Wanna get the certain item in a vector, type {get vector1 1}")
   (println "Wanna add to a vector, type {conj vector1 1}")
   (println "Wanna pop the last value of the vector off, type {pop vector1}")
   (println "Wanna get the vector of a vector, type {subvec vector1 1 3}")))

(defn messWithMyHashMap
  ([]
   (println "A hash-map is a key value paired item (like a dictionary in C#)")
   (println "Wanna see the hash-map, type {println hashmap1}")
   (println "Wanna try a sorted list, type {println (sorted-map \"Name\" \"James\" \"Age\" 26")
   (println "Wanna try another sorted list, type {println (sorted-map 3 \"Don't be his friend. You know you're gonna wake up in his bed in the morning\" 2 \"Don't let him in. You'll have to kick him out again\" 1 \"Don't pick up th phone. You know he's only calling 'cause he's drunk and alone\"")
   (println "Wanna get the value of a key, type {get hashmap1 :a}")
   (println "Wanna find the key/value pair using a key, type {find hashmap1 :a}")
   (println "Wanna see if a hash-map contains a value, type {contains? hashmap1 :a}")
   (println "Wanna get the keys of a hash-map, type {keys hashmap1}")
   (println "Wanna get the values of a hash-map, type {vals hashmap1}")
   (println "Wanna merge two hash-maps, type {merge-with + hashmap1 (hash-map :f \"It's been merged\"")))

(defn atom-ex
  ([]
   (println "An atom is a changeable value in clojure")
   (println "To run an example of what an atom does, re-run this method with a single parameter"))
  ([x]
   (def atomEx (atom x))
   (add-watch atomEx :watcher
     (fn [key atom old-state new-state]
       (println "atomEx changed from " old-state " to " new-state)))
   (println "The 1st value of the parameter is" @atomEx)
   (reset! atomEx 10)
   (println "The parameter has now been changed to" @atomEx)
   (swap! atomEx inc)
   (println "The parameter has been incremented" @atomEx)))

(defn agent-ex
  ([]
   (println "An agent is another changeable value in clojure")
   (println "You'll need to make sure to await for changes when manipulating an agent")
   (def ticketsSold (agent 0))
   (println @ticketsSold)
   (send ticketsSold + 15)
   (println "You can wait with a println")
   (println "Tickets" @ticketsSold)
   (send ticketsSold + 10)
   (println "Or with an await-for command")
   (await-for 100 ticketsSold)
   (println "Tickets" @ticketsSold)
   (println "Make sure to shutdown your agents once you're done")
   (shutdown-agents)))

(defn math-functions
  ([]
   (println "Wanna do an add {+ 1 2 3}")
   (println "Wanna do an add {- 1 2 3}")
   (println "Wanna do an add {* 1 2 3}")
   (println "Wanna do an add {/ 1 2}")
   (println "Wanna do an add {mod 2 3}")
   (println "Wanna do an add {inc 3}")
   (println "Wanna do an add {dec 3}")
   (println "Wanna do an add {Math/abs -10}")
   (println "Wanna do an add {Math/cbrt 8}")
   (println "Wanna do an add {Math/sqrt 64}")
   (println "Wanna do an add {Math/ceil 10.1}")
   (println "Wanna do an add {Math/floor 10.1}")
   (println "Wanna do an add {Math/exp 1")
   (println "Wanna do an add {Math/hypot 3 4}")
   (println "Wanna do an add {Math/log 2}")
   (println "Wanna do an add {Math/log10 2}")
   (println "Wanna do an add {Math/max 1 5}")
   (println "Wanna do an add {Math/min 1 5}")
   (println "Wanna do an add {Math/pow 2 2}")
   (println "Wanna do an add {rand-int 100}")
   (println "Wanna do an add {reduce + [10 20 30 40]}")
   (println "Wanna do an add {Math/PI}")))

(defn get-sum
  "Gets the sum of two, three, four, or five numbers"
  ([v w]
   (+ v w))
  ([v w x]
   (+ v w x))
  ([v w x y]
   (+ v w x y))
  ([v w x y z]
   (+ v w x y z)))

(defn hello-you
  ([name]
   (str "Hello " name)))

(defn hello-you-repeated
  ([& names]
   (map hello-you names)))

(defn can-vote
  ([age]
   (if (>= 18 age)
     (println "You can vote because you are" age)
     (println "You cannot vote you're only" age))))

(defn what-can-i-do
  ([age]
   (if (< 16 age)
     (do (println "You can't drive")
       (println "You can't vote"))
     (println "Who knows what you can do"))))

(defn when-ex
  ([trueOrFalse]
   (when trueOrFalse
     (println "This must've been true")
     (println "This must've been false"))))

(defn what-can-i-really-do
  ([age]
   (cond 
     (< age 5) (println "You can go to kindergarten")     
     (and (>= age 5) (< age 10)) (println "You can go to primary school")
     (and (>= age 10) (< age 18)) (println "You get to go to highschool")
     :else (println "You can do anything"))))

(defn one-to-x
  ([x]
   (def i (atom 1))
   (while (<= @i x)
     (do
       (println @i)
       (swap! i inc)))))

(defn dbl-to-x
  ([x]
   (dotimes [i x]
     (println (* i 2)))))

(defn trp-to-x
  ([x y]
   (loop [i x]
     (when (< i y)
       (println (* i 3))
       (recur (+ i 1))))))

(defn print-list
  [& nums]
  (doseq [x nums]
    (println x)))

(defn write-to-file
  [file text]
  (with-open [wrtr (writer file)]
    (.write wrtr text)))

(defn read-from-file
  [file]
  (try
    (println (slurp file))
    (catch Exception e (println "Error"))))

(defn append-to-file 
  [file text]
  (with-open [wrtr (writer file :append true)]
    (.write wrtr text)))

(defn read-line-from-file
  [file]
  (with-open [rdr (reader file)]
    (doseq [line (line-seq rdr)]
      (println line))))

(defn destruct
  []
  (def vectVals [1 2 3 4])
  (let [[one two & the-rest] vectVals]
    (println one two the-rest)))

(defn struct-map-ex
  []
  (defstruct Customer :Name :Phone)
  (def cust1 (struct Customer "James" 5555555))
  (def cust2 (struct Customer "Beck" 6666666))
  
  (println cust1)
  (println (:Name cust2)))

(defn map-values-square
  []
  (map (fn [x] (* x x)) (range 1 10)))

(defn map-values-3
  []
  (map #(* % 3) (range 1 10)))

(defn multiply
  [x y z]
  (#(* %1 %2 %3) x y z))

(defn custom-multiplier
  [mult-by]
  #(* % mult-by))

(def mult-by-3
  (custom-multiplier 3))

(defn output-mult
  []
  (mult-by-3 3))

(defn filter-list
  [& list]
  (println (take 2 list))
  (println (drop 1 list))
  (println (take-while neg? list))
  (println (drop-while neg? list))
  (println (filter #(> % 2) list)))

(defmacro discount
  ([cond dis1 dis2]
   (list `if cond dis1 dis2)))

(defmacro reg-math
  [calc]
  (list (second calc) (first calc) (nth calc 2)))

(defmacro do-more
  [cond & body]
  (list `if cond (cons 'do body)))

(defmacro do-more-2
  [cond & body]
  `(if ~cond (do ~@body)))
